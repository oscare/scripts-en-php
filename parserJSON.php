<?php

	function getFileContent($file) {
		/*
			Funcion que obtiene el conenido del archivo de entrada a
			convertir a JSON
		*/
		$f = fopen($file, 'r');
		$content = fread($f, filesize($file));
		fclose($f);

		//Eliminamos el salto de linea al final del string
		if (strcmp($content[strlen($content) - 1], '\n')){
			$content = substr($content, 0, -1);
		}

		return $content;
	}

	function splitString($content) {
		/*
			Funcion que obtiene en un arreglo los elementos llave:valor
			leidos del archivo.
			Estos deben estar separados por comas de la siguiente forma:
			llave1:valor1,llave2:valor2.
			No considera las ',' que hay dentro de corchetes [] o parentesis {}.
		*/

		$json_array = array();
		$value = '';
		//Bandera para considerar si se esta leyendo un subarray del json
		$isArray = 0;
		//El string debe iniciar y terminar con {} para considerarlo valido
		if (strcmp($content[0], '{') && strcmp($content[strlen($content) - 1], '}')) {
			return False;
		}
		//Cortamos las llaves del json principal
		$content = substr($content, 1, -1);
		
		for ($i = 0; $i < strlen($content); $i ++) {
			//Si abre un corchete o llave se considera subarray
			if (strcmp($content[$i], '[') == 0 || strcmp($content[$i], '{') == 0)
				$isArray = 1;
			//Si se cierra corchete o llave se deja de considerar subarray
			if (strcmp($content[$i], ']') == 0 || strcmp($content[$i], '}') == 0)
				$isArray = 0;

			if (strcmp($content[$i], ',') == 0) {
				//Si no estamos en un subarray, agregamos $value que tiene
				//llave:valor al array e inicializamos $value para el siguiente
				//llave:valor
				if (!$isArray){
					array_push($json_array, $value);
					$value = '';
				}
				//Si estamos en un subarray, no se hace split con esa coma
				else {
					$value .= $content[$i];
				}
			}
			else {
				$value .= $content[$i];
			}

			//Si se llego al final de la cadena se agrega el ultimo llave:valor
			//al array
			if($i == strlen($content) - 1){
				array_push($json_array, $value);
				$value = '';
			}
		}

		//Retornamos un array de llaves y sus respectivos valores aun
		//de tipo string
		return $json_array;
	}

	function getValueJSON($value) {
		/*
			Funcion que devuelve un array asociativo de llave:valor
		*/
		//Quitamos los parentesis {}
		$value = substr($value, 1, -1);
		$arr = explode(',', $value);
		$valueArray = array();
		//Recorremos el array
		for ($i = 0; $i < count($arr); $i ++){
			//Cada elemento le hacemos split con ':'
			$tmp = explode(':', $arr[$i]);
			//Quitamos las comillas leidas del archivo
			if ($tmp[0][0] == '"'){
				$tmp[0] = substr($tmp[0], 1, -1);
			}
			if ($tmp[1][0] == '"'){
				$tmp[1] = substr($tmp[1], 1, -1);
			}
			//Agregamos un nuevo elemento al array asociativo
			$valueArray[$tmp[0]] = $tmp[1];
		}
		
		return $valueArray;
	}

	function getValueArray($value) {
		/*
			Funcion que devuelve un array no asociativo, es decir, una lista
		*/
		//Quitamos los corchetes []
		$value = substr($value, 1, -1);
		//Obtenemos los elementos de la lista
		$arr = explode(',', $value);
		//Quitamos las comillas a cada elemento leidas del archivo
		for ($i = 0; $i < count($arr); $i ++){
			if ($arr[$i][0] == '"'){
				$arr[$i] = substr($arr[$i], 1, -1);
			}
		}

		return $arr;
	}

	function getKeyValueSubArray($keyValueString, $isJSON) {
		/*
			Funcion que devuelve un array asociativo con un solo elemento.
			Se utiliza para obtener arreglos anidados en el arreglo principal.
		*/
		$pos = strpos($keyValueString, ':');
		//Obtenemos la llave a partir de la posicion de ':' y le quitamos
		//las comillas leidas del archivo
		$key = substr($keyValueString, 0, $pos);
		if ($key[0] == '"'){
			$key = substr($key, 1, -1);
		}
		//Obtenemos el valor asociado a partir de la posicion de ':'.
		//Dependiendo de si es un arreglo asociativo (json) o una lista
		//se forma ese subarreglo
		$value = substr($keyValueString, $pos + 1);
		if ($isJSON == 1)
			$value = getValueJSON($value);
		else
			$value = getValueArray($value);

		$subArray = array();
		array_push($subArray, $key);
		array_push($subArray, $value);

		return $subArray;
	}

	function array2JSON($json_array) {
		/*
			Funcion que obtiene un arreglo asociativo (JSON) a partir de un
			string dado como parametro.
			Este string debe cumplir ciertas reglas como:
			-Los elementos deben ir separados por comas.
			-No debe haber ni un espacio (caracter) entre elementos ni entre
				llaves y sus respectivos valores.

			El arreglo que se forma tiene las siguientes caracteristicas:
			-Permite dos arreglos anidados, es decir, el principal y un subarreglo.
			-Tanto llaves como valores son de tipo string, a menos que un valor
				sea un arreglo, en este caso, es de tipo array y sus elementos
				son de tipo string
			-distingue entre subarreglos asosiativos (json) 
				y no asociativos (listas)
		*/
		$json = array();

		for ($i = 0; $i < count($json_array); $i ++) {
			//Si el elemento tiene un subarreglo asociativo, entonces se
			//obtiene este subarreglo ya procesado
			if (preg_match('/{.+}/', $json_array[$i])) {
				$keyValue = getKeyValueSubArray($json_array[$i], 1);
			}
			//Si el elemento tiene un subarreglo no asociativo, entonces se
			//obtiene este subarreglo ya procesado
			elseif (preg_match('/\[.+\]/', $json_array[$i])) {
				$keyValue = getKeyValueSubArray($json_array[$i], 0);
			}
			//Si solo es un elemento llave:valor, se procesa
			else {
				$keyValue = explode(':', $json_array[$i]);
				//Quitamos las comillas leidas del archivo
				if ($keyValue[0][0] == '"'){
					$keyValue[0] = substr($keyValue[0], 1, -1);
				}
				if ($keyValue[1][0] == '"'){
					$keyValue[1] = substr($keyValue[1], 1, -1);
				}
			}

			//A cada iteracion se agrega el elemento al arreglo principal
			$json[$keyValue[0]] = $keyValue[1];
			//var_dump($keyValue);
		}

		return $json;
	}

	function getJSONObject($inputFile){
		/*
			Funcion que retorna un array asociativo (JSON)
			de un archivo dado como parametro
		*/
		$arreglo = splitString(getFileContent($inputFile));
		$JSONobject = array2JSON($arreglo);

		return $JSONobject;
	}

	$myJSON = getJSONObject($argv[1]);	
	var_dump($myJSON);

	/* Archivo de prueba: 
	{"nombre":"Oscar","apellido":"Espinosa","edad":21,"becario":"si","pertenencias":["celular","cartera","reloj"],"numeros":{"uno":1,"dos":2,"tres":3},"otroCampo":"si"}
	*/
?>