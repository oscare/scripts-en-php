#!/usr/bin/php
<?php
	//Autor: Oscar Espinosa Curiel
	
	require_once('display_error.php');
	require_once '/var/www/google-api-php-client-2.2.2/vendor/autoload.php';

	class GoogleLogin {
		private $clientId;
		private $clientSecret;
		private $redirectURL;
		private $client;
		private $plus;
		public $state;
		public $hReflogin;

		public function __construct($client_ID, $client_secret, $redirect_URL = NULL) {
			$this->clientId = $client_ID;
			$this->clientSecret = $client_secret;

			if ($redirect_URL != NULL)
				$this->redirectURL = $redirect_URL;
			else
				$this->redirectURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		}

		public function logIn() {
			/*
				Metodo que establece un inicio de sesion con la API Google
			*/
			//Si ya existe una sesion activa, se redirecciona automaticamente a
			//ingresarOO.php
			if(!session_id()){
				session_start();
			}
			if(isset($_SESSION['username']) || isset($_SESSION['access_token'])) {
				header("Location: ingresarOO.php");
			}

			$this->client = new Google_Client();
			//nombre de aplicacion que se puso a la API
			$this->client->setApplicationName("Cliente Web 1");
			$this->client->setClientId($this->clientId);
			$this->client->setClientSecret($this->clientSecret);
			//para saber a donde regresar despues del login de google
			$this->client->setRedirectUri($this->redirectURL);
			$this->client->setScopes(array(Google_Service_Plus::PLUS_ME));
			$this->plus = new Google_Service_Plus($this->client);

			//Se establece una sesion activa con la sesion de Google
			if (isset($_GET['code'])){
				if (strval($_SESSION['state']) !== strval($_GET['state'])) {
					echo 'El estado de la sesion no coincide.';
					exit(1);
				}
				$this->client->authenticate($_GET['code']);
				$_SESSION['access_token'] = $this->client->getAccessToken();
				$_SESSION['info_user'] = $this->plus->people->get('me');
				header('Location: ingresarOO.php');
			}

			if(isset($_SESSION['access_token'])) {
				$this->client->setAccessToken($_SESSION['access_token']);
			}

			$this->state = mt_rand();
			$this->client->setState($this->state);
			$_SESSION['state'] = $this->state;
			$this->hReflogin = $this->client->createAuthUrl();
		}
	}

	$myClientID = "";
	$myClientSecret = "";
	$login = new GoogleLogin($myClientID, $myClientSecret);

	$login->logIn();
	//echo $login->hReflogin;
?>
<!DOCTYPE html>
<html>
	<script src="https://apis.google.com/js/platform.js" async defer></script>

	<body>
		<form method="POST" action="ingresarOO.php">
			<p>Usuario: </p>
			<input type="text" name="user" >
			<p>Contrasenia: </p>
			<input type="text" name="pass">
			<br/>
			<input type="submit" name="entrar" value="ingresar">
		</form>
		<br><p>O ingresa con google plus</p><br>

		<div class="g-signin2" data-onsuccess="onSignIn">
			<div style="height:36px;width:120px;" class="abcRioButton abcRioButtonLightBlue">
				<a href="<?php echo $login->hReflogin;?>">
					<div class="abcRioButtonContentWrapper">
						<div class="abcRioButtonIcon" style="padding:8px">
							<div style="width:18px;height:18px;" class="abcRioButtonSvgImageWithFallback abcRioButtonIconImage abcRioButtonIconImage18">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 48 48" class="abcRioButtonSvg">
									<g>
										<path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path>
										<path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path>
										<path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path>
										<path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path>
										<path fill="none" d="M0 0h48v48H0z"></path>
									</g>
								</svg>
							</div>
						</div>
						<span style="font-size:13px;line-height:34px;" class="abcRioButtonContents">
							<span id="not_signed_inj22euyrg4xue">Inicia sesión</span>
							<span id="connectedj22euyrg4xue" style="display:none">Signed in</span>
						</span>
					</div>
				</a>
			</div>
		</div>

	</body>
</html>


