#!/usr/bin/php
<?php
	//Autor: Oscar Espinosa Curiel

	require_once('display_error.php');

	class Login {
		private $user;
		private $pass;
		public static $host  = "127.0.0.1";
		public static $port = "5432";

		public function __construct($usr, $passwd) {
			$this->user = $usr;
			$this->pass = hash('sha256',$passwd);
		}

		public function connect2DB() {
			/*
				Metodo para hacer la conexion con la base de datos.
				Retorna un socket con la conexion establecida o Falso si no
				se pudo hacer la conexion.
			*/
			$conn_str = "host='".self::$host."' port='".self::$port."' dbname=bec13g user=admin password=hola123,";
			$dbconn = pg_connect($conn_str) or die (False);
		    
		    return $dbconn; 
		}

		public function getQuery() {
			/*
				Metodo para hacer una consulta a la base de datos.
				Retorna el valor de la consulta o un False en caso de error.
			*/
			$connection = $this->connect2DB();
			$result = pg_query_params($connection, "SELECT nombre, password FROM becarios WHERE nombre=$1 AND password=$2", array($this->user, $this->pass));
		        if(!$result){
                		echo "Ocurrio un error.<br>";
                		return False;
        		}
        		else{
        			return $result;
        		}
			pg_close ($connection);
		}

		public function showQuery(){
			/*
				Metodo para mostrar el nombre y el hash de la contrasena
				de la consulta hecha a la base de datos
			*/
			$result = $this->getQuery();
			while ($query = pg_fetch_assoc($result))
			{
				//echo "id: ". $query['id']."<br>";
				echo "Nombre: ".$query['nombre']."<br>";
				echo "password: ".$query['password']."<br>";
			}
		}

		public function setSession() {
			/*
				Metodo que establece la sesion activa con el usuario y
				el hash de la contrasena obtenidas de la base de datos.
			*/
			if(!session_id()){
				session_start();
				$status = $this->getQuery();
				if($status != False) {
					$this->showQuery();
					$_SESSION["username"] = $this->user;
					$_SESSION["hashPass"] = $this->pass;
				}
				else {
					echo "<br>No se ha iniciado sesion<br>";
				}
			}
		}

		public function logout() {
			/*
				Metodo que cierra la sesion activa en el navegador
			*/
			unset($_SESSION['access_token']);
			unset($_SESSION['info_user']);
			unset($_SESSION['username']);
			unset($_SESSION['hashPass']);
			session_unset();
			session_destroy();
		}

		public function logUp() {
			/*
				Metodo que hace un nuevo registro en la base de datos
			*/
			$connection = $this->connect2DB();
			if ($this->getQuery() == NULL){
				$res=pg_insert($connection,'becarios', array("nombre" => $this->user, "password"=> $this->pass));

				if($res){
					echo "<h2>Se registro en la base</h2> <br><br>";
				}else{
					echo "<h2>Ocurrio un error</h2> <br><br>";
				}

				pg_close ($connection);
			}
			else {
				echo "<h2>Este usuario ya existe en la base</h2> <br><br>";
			}
			
		}

		public function __str() {
			/*
				Sobreescritura del metodo para imprimir los valores del
				objeto
			*/
			return $this->user."-".$this->pass."-".self::$host."-".self::$port;
		}
	}




	if(!empty($_POST)){
		$log = new Login($_POST["user"], $_POST["pass"]);
		$log->setSession();
	}

	if(isset($_REQUEST['logout'])) {
		if(isset($log)) {
			$log->logout();
			
		}

		else {
			session_start();
			unset($_SESSION['access_token']);
			unset($_SESSION['user_id']);
			session_unset();
			session_destroy();
			header('Location: google_login.php');
		}
			
	}

?>


<!DOCTYPE html>
<a href="<?php $_SERVER['PHP_SELF'] ?>?logout">Salir</a>