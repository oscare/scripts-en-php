#!/usr/bin/php
<?php
	//Autor: Oscar Espinosa Curiel

	//Directorio donde se encuentra el modulo de PHPMailer
	require '/var/www/PHPMailer/src/Exception.php';
	require '/var/www/PHPMailer/src/PHPMailer.php';
	require '/var/www/PHPMailer/src/SMTP.php';

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	class Mail {
		public $msg_salida;
		public $estilo;
		public $from;
		public $smtp_server;
		public $port;
		public $pass;
		public $to;
		public $subject;
		public $mensaje;

		public function __construct() {

			$this->msg_salida = 'mensaje';
			$this->estilo = 'background-color:blue; color:white;';

			$this->from = $_POST['from'];
			$this->smtp_server = $_POST['smtp_server'];
			$this->port = $_POST['port'];
			$this->pass = $_POST['pass'];
			$this->to = $_POST['to'];
			$this->subject = $_POST['subject'];
			$this->mensaje = $_POST['mensaje'];
		}

		public function sendMail() {
			/*
				Metodo que envia los correos con los parametreos establecidos
			*/
			try{
				//true es para usar todas las excepciones de esta clase
				$mail = new PHPMailer(true);
				//modo verboso devuelve errores en string
				$mail->SMTPDebug = 2;
				$mail->isSMTP();
				$mail->Host = $this->smtp_server;
				$mail->SMTPAuth = true;
				$mail->Username = $this->from;
				$mail->Password = $this->pass;
				//Google solo permite envio por tls puerto:587
				$mail->SMTPSecure = 'tls';
				$mail->Port = $this->port;

				$mail->setFrom($this->from);
				$mail->addAddress($this->to);
				$mail->Subject = $this->subject;
				$mail->Body = $this->mensaje;

				//Enviamos archivo solo si se selecciona un archivo
				if ($_FILES['uploaded_file']['name']) {
					var_dump($_FILES['uploaded_file']['tmp_name']);
					$mail->AddAttachment($_FILES['uploaded_file']['tmp_name'], $_FILES['uploaded_file']['name']);	
				}

				//Enviamos el correo usando send, devuelve todo el log por el modo verboso
				ob_start();
				$mail->send();
				$this->msg_salida = ob_get_clean();
				$this->estilo = "background-color:blue; color:white;";

			}
			catch(Exception $e){
				$this->estilo = "background-color:red; color:white;";
				$this->msg_salida = $mail->ErrorInfo;
			}
		}
	}

	$mail = new Mail();
	$mail->sendMail();
	
?>

<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<title>CSI/UNAM-CERT</title>
		<link rel="stylesheet" href="CSI_UNAM-CERT_files/style.css">
		<link rel="shortcut icon" href="https://132.247.146.130/loginApp/img/dsc.ico" type="image/x-icon">
	</head>

	<body>
		<div id="divError" style="<?php echo $mail->estilo;?>">
			<p><?php echo $mail->msg_salida;?></p>
		</div>
		<div class="form">
			<h2>Enviar correo electronico</h2>
			<!-- action usamos el mismo archivo para procesar la info-->
			<!--enctype lo agregamos para el envio de archivos-->
			<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" enctype='multipart/form-data'>
				<input class="campo" name="from" placeholder="De: " value="obecario@gmail.com" type="text">
				<input class="campo" name="smtp_server" placeholder="Servidor smtp" value="smtp.gmail.com" type="text">
				<input class="campo" name="port" placeholder="Puerto" value="587" type="text">
				<input class="campo" name="pass" placeholder="Contraseña" value="hola123," type="password">
				<input class="campo" name="to" placeholder="Para: " value="bec13g@gmail.com" type="text">
				<input class="campo" name="subject" placeholder="Asunto: " value="Prueba" type="text">
				<textarea class="campo" name="mensaje" placeholder="Mensaje" rows="6">Correo de prueba</textarea>
				<input class="campo" type="file" name="uploaded_file" id="uploaded_file" />
				<div id="formsubmitbutton">
					<input class="boton" name="sendEmail" value="enviar" type="submit">
				</div>
			</form>
		</div>
	

</body></html>