#!/usr/bin/php
<?php 
	//Autor: Oscar Espinosa Curiel

	//Esta linea es para no mostrar los warning que salen con
	//los tag header, nav y footer en el metodo loadHTML
	error_reporting(E_ERROR);

	class CrawlerAnalizer {
		public $links_visited = array();

		public function doCrawler($url_root, $file, $max_deep = 2) {

			if ($max_deep <= 0 || $max_deep > 5)
				$max_deep = 2;

			$fp = fopen($file, 'w');

			echo "working...\n";
			$this->getSubLinks($fp, $url_root, $max_deep);

			fclose($fp);
		}

		public function getSubLinks($fp, $link, $deep) {
			/*
				Metodo recursivo que obtiene todos los enlaces a otra pagina web dentro
				de la url dada como parametro.
				Agrega cada enlace al archivo mandado como parametro (socket abierto). 

			*/
			//Condicion de paro de la recursividad:
			// cuando ya se visito el enlace o cuando se a alcanzado
			//la profundidad limite
			if (isset($this->links_visited[$link]) || $deep == 0){
				return True;
			}
			echo "...\n";
			$this->links_visited[$link] = True;
			fwrite($fp, $link.PHP_EOL);
			
			$doc =  new DomDocument();
			//Para evitar algunos enlaces 'rotos' se puso el condicional
			if (file_get_contents($link) != false) {
				$doc->loadHTML(file_get_contents($link));
				//Obtenemos las etiquetas anchor en el documento
				$atag = $doc->getElementsByTagName("a");

				foreach ($atag as $key) {
					$child_link = $this->digestURL($key->getAttribute('href'), $link);
					if ($child_link != null) {
							//Se hace la recursividad para cada enlace dentro
							//de la pagina actual modificando la profundidad
							$this->getSubLinks($fp, $child_link, $deep - 1);
					}
				}
			}
		}

		public function digestURL($url, $parent_url) {
			/*
				Metodo que devuelve una url valida. Elimina las referencias
				nulas (#), correos (mailto) y adecua las referencias relativas
				a la misma pagina web agregando la direccion url padre.
				Se omiten enlaces a google para no perder el enfoque en la pagina.
				Los enlaces a Linkedin se omiten porque generan error en file_get_contents
			*/
			if (substr($url, 0, 1) == '/') {
				$url_parts = explode('/', $parent_url);
				return $url_parts[0]."//".$url_parts[2].$url;
			}
			elseif (substr($url, 0, 1) == '#' || substr($url, 0, 6) == 'mailto' || strpos($url, 'www.google.com') != false || strpos($url, 'www.linkedin.com') != false) {
				return null;
			}
			else {
				return $url;
			}
		}
	}

	$url = $argv[1];
	$file = $argv[2];
	$crawler = new CrawlerAnalizer();
	$crawler->doCrawler($url, $file, 2);

?>php